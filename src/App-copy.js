import React,{Fragment} from 'react';
import Recuperar from './components/Login/Recuperarcontrasena';
import{
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import { Container } from 'reactstrap';
import Footer from './components/footer/Footer';
import Login from './components/Login/Login';
import Listas from './components/Listas';
import Formulario from './components/Formulario';

{/*import Header from './components/header/Header';
import Nav from './components/nav/Nav';
import Sidebar from './components/sidebar/Sidebar';
import Container from './components/container/Container';*/}


export default function App() {
  let objHeader={
    titulo:"Primer Front con React",
    parrafo:"Este es un sitio responsivo"
  }
// sin await y async
{/*fetch("https://pokeapi.co/api/v2/pokemon/")
  .then(res => res.json())
  .then(data => {
    let arrayNombres=[];
    data.results.forEach((element) => {
      arrayNombres.push(element.name);
    });
   // console.log(arrayNombres);
  })
.catch((error) => console.log(error));*/}
// con await y async , map y filter
 {/* const obtenerPokemones = async () => {
    try {
      const res = await fetch("https://pokeapi.co/api/v2/pokemon/");
      const data = await res.json();
      //console.log(data.results);
      //const arrayNombre=data.results.map(poke=>poke.name);
      //const arrayNombre=data.results.filter(poke=>poke.name==='bulbasaur');
      const arrayNombre=data.results.filter(poke=>poke.name!=='bulbasaur');
      console.log(arrayNombre);
    } catch (error) {
      console.log(error);
    }
  };
obtenerPokemones();*/}



  const fecha = new Date().getFullYear();
  return (
    <Router>
      <div className="container col-12">
        <Switch>
        <Route path="/" exact>
            <Login/>
            <Formulario/>
          </Route>
          <Route path="/recuperar" >
          <Recuperar/>
          </Route>
          
          
        </Switch>
       
        { /* <Fragment>
          <Header titulo={objHeader.titulo} parrafo={objHeader.parrafo}/>
        </Fragment>*/}
        <Footer fecha={fecha}/>
        </div>
    </Router>
  
    
    );
}


