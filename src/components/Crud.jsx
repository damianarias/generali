import React,{useState} from 'react';
import shortid from 'shortid';

function Crud() {

    const [tarea,setTarea]=useState('');
    const [lista,setLista]=useState([]);
    const [modoEdicion,setModoEdicion]=useState(false);
    const [id,setId] = useState('');
    const [error,setError] = useState(null);

    const agregarTarea = (e)=>{
        e.preventDefault();
        if(!tarea.trim()){
            setError('Escriba algo por favor');
            return;
        }
        
        setLista([
            ...lista,
            {id:shortid.generate(),nombreTarea:tarea}
        ]);
        setTarea('');
    }
    const eliminarTarea=(id)=>{
        const arrayFiltrado = lista.filter(item=> (item.id!==id));
        setLista(arrayFiltrado);
    }

    const editar=(item)=>{
        setModoEdicion(true);
        setTarea(item.nombreTarea);
        setId(item.id);
    }

    const editarTarea = (e)=>{
        e.preventDefault();
        if(!tarea.trim()){
            setError('Escriba algo por favor');
            return;
        }
        const arrayEditado = lista.map(item=>(item.id===id)?{id:id,nombreTarea:tarea}:item)
        setLista(arrayEditado);
        setModoEdicion(false);
        setTarea('');
        setId('');
    }
    return (
        <div className="row">
            <div className="col-8">
                <h4 className="text-center">Lista de Tareas</h4> 
                <ul className="list-group mb-2">
                {
                    lista.length===0 ?(
                        <li className="list-group-item">No hay Tareas</li>
                    ):(
                        lista.map(item=>(
                            <li className="list-group-item" key={item.id}>
                                <span  className="lead">{item.nombreTarea}</span>
                                <button 
                                    className="btn btn-danger btn-sm float-right mx-2" 
                                    onClick={()=>eliminarTarea(item.id)}>Eliminar</button>
                                <button 
                                    className="btn btn-warning btn-sm float-right mx-2"
                                    onClick={()=>editar(item)}
                                    >Editar</button>
                            </li>                
                        ))
                    )
                    
                }
                    
                </ul>
            </div>
            <div className="col-4">
            <h4 className="text-center">
                {
                    modoEdicion?'Editar tarea':'Agregar Trea'
                }    
            </h4> 
            <form onSubmit={modoEdicion? editarTarea : agregarTarea}>

                {
                    error ? <span className="text-danger">{error}</span>:null
                }
                <input 
                    type="text" 
                    className="form-control mb-2"
                    placeholder="Ingrese tarea.."  
                    onChange={e => setTarea(e.target.value)}  
                    value={tarea}
                />
                {
                    modoEdicion?(
                        <button className=" btn btn-dark btn-block mb-2" type="submit">Editar</button>
                    ):(
                        <button className=" btn btn-dark btn-block mb-2" type="submit">Agregar</button>
                    )
                }
               
            </form>
            </div>
        </div>
    )
}

export default Crud
