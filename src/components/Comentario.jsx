import React from 'react'

const Comentario = ({urlImagen,persona}) => {
    return (
        <div className="media">
            <img src={urlImagen} className="mr-3" alt="" width='20%'/>
            <div className="media-body">
                <h5 className="mt-0"> {persona}</h5>
            </div>
        </div>
    )
}
export default Comentario;
