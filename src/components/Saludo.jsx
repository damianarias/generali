import React from 'react'

const Saludo = (props) => {
    return (
        <div className="container mt-5">
            <h2>
                Saludando a todos {props.persona} {props.edad}
            </h2>
        </div>
    )
}
export default Saludo;