import React,{Fragment, useState} from 'react'

function Formulario() {

    const[fruta,setFruta]=useState('');
    const [descripcion,setDescripcion]=useState('');
    const [lista,setLista]= useState([]);
    const procesarDatos =(e)=>{
        e.preventDefault();
        
        if(!fruta.trim()){
            console.log('esta vacio fruta');
            return
        }
        if(!descripcion.trim()){
            console.log('esta vacio desc');
            return
        }

        setLista([
            ...lista,
            {frut:fruta,decp:descripcion}
        ]);

        console.log('guardando datos....');
        e.target.reset()
        setFruta('');
        setDescripcion('');

    }

    return (
        <div>
            <h2>Formularios</h2>
            <form onSubmit={procesarDatos}>
                <input
                    type="text"
                    placeholder="ingrse un registro"
                    className="form-control mb-2"
                    onChange={e =>(setFruta(e.target.value))}
                />
                <input
                type="password"
                placeholder="password"
                className="form-control mb-2"
                onChange={ e=>setDescripcion(e.target.value)}
                />
                <button className="btn btn-primary btn-block" type="submit">INGRESAR</button>
            </form>      
            <Fragment>
             {
                  lista.map((item,index) =>(
                      <p key={index}> {item.frut}</p>
                      ))
              }
         </Fragment>
        </div>
         
          
    )
}

export default Formulario
