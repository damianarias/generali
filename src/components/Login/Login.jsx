import AuthForm, { STATE_LOGIN } from "./AuthForm";
import React from "react";
import { Card, Col, Row } from "reactstrap";
import "../../styles/estilos.css";
import "./login.css";

class AuthPage extends React.Component {
  handleAuthState = authState => {
    if (authState === STATE_LOGIN) {
      this.props.history.push("/login");
    } else {
      this.props.history.push("/signup");
    }
  };

  handleLogoClick = () => {
    this.props.history.push("/");
  };

  render() {
    return (
      <main className="bg-light">
        <Row
          style={{
            height: "100vh",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Col md={6} lg={4}>
            <Card body>
              <AuthForm
                authState={this.props.authState}
                onChangeAuthState={this.handleAuthState}
                onLogoClick={this.handleLogoClick}
              />
            </Card>
          </Col>
        </Row>
      </main>
    );
  }
}

export default AuthPage;
