import logo from "../../assets/img/logo/logo-generali.png";
import PropTypes from "prop-types";
import React from "react";
import { Button, Form, FormGroup, Input, Label } from "reactstrap";

class AuthForm extends React.Component {
  get isLogin() {
    return this.props.authState === STATE_LOGIN;
  }

  changeAuthState = authState => event => {
    event.preventDefault();

    this.props.onChangeAuthState(authState);
  };

  handleSubmit = event => {
    event.preventDefault();
  };

  // EJEMPLO DE onClick
    

  render() {
    const { showLogo, onSelectIniciarSesion, onChangeFormulario } = this.props;
  const eventoClick = () =>{
    console.log('me diste click');
  }
    return (
      <Form onSubmit={this.handleSubmit}>
        {showLogo && (
          <div className="text-center pb-4">
            <img
              src={logo}
              className="login-logo"
              style={{ cursor: "pointer" }}
              alt="logo"
            />
          </div>
        )}
        <FormGroup>
          <Label for="Correo Electrónico">Correo Electrónico</Label>
          <Input
            type="email"
            id="Inputcorreo"
            placeholder="jperez@email.com"
            onChange={onChangeFormulario}
          />
        </FormGroup>
        <FormGroup>
          <Label for="Contraseña">Contraseña</Label>
          <Input
            type="password"
            id="Inputcontrasena"
            placeholder="Contraseña"
            onChange={onChangeFormulario}
          />
        </FormGroup>
        <FormGroup check>
          <Label check>
            <Input type="checkbox" /> "Recordar datos"
          </Label>
        </FormGroup>
        <hr />
        <Button
          size="lg"
          className="bg-gradient-theme-left border-0"
          block
          onClick={()=>eventoClick()}
        >
          INGRESAR
        </Button>
        <div className="text-center pt-1">
          <h6>
            <a href="/recuperar" onClick={onSelectIniciarSesion}>
              <p />
              Olvidé mi Contraseña
            </a>
          </h6>
        </div>
      </Form>
    );
  }
}

export const STATE_LOGIN = "LOGIN";

AuthForm.propTypes = {
  authState: PropTypes.oneOf([STATE_LOGIN]).isRequired,
  onSelectIniciarSesion: PropTypes.func.isRequired,
  showLogo: PropTypes.bool
};

AuthForm.defaultProps = {
  authState: "LOGIN",
  showLogo: true
};

export default AuthForm;
