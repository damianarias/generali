import logo from "assets/img/logo/logo_aig_login.png";
import PropTypes from "prop-types";
import React from "react";
import { Button, Form, FormGroup, Input, Label } from "reactstrap";

class AuthForm extends React.Component {
  get isLogin() {
    return this.props.authState === STATE_LOGIN;
  }

  get isSignup() {
    return this.props.authState === STATE_SIGNUP;
  }

  changeAuthState = authState => event => {
    event.preventDefault();

    this.props.onChangeAuthState(authState);
  };

  handleSubmit = event => {
    event.preventDefault();
  };

  renderButtonText() {
    const { buttonText } = this.props;

    if (!buttonText && this.isLogin) {
      return "ENVIAR CONTRASEÑA";
    }

    if (!buttonText && this.isSignup) {
      return "Signup";
    }

    return buttonText;
  }

  render() {
    const {
      showLogo,
      usuarioLabel,
      usuarioInputProps,
      children,
      onLogoClick
    } = this.props;

    return (
      <Form onSubmit={this.handleSubmit}>
        {showLogo && (
          <div className="text-center pb-4">
            <img
              src={logo}
              className="login-logo"
              style={{ cursor: "pointer" }}
              alt="logo"
              onClick={onLogoClick}
            />
          </div>
        )}
        <div>
          <h2>Recuperar Contraseña</h2>
          <div>
            Por favor ingresa tu correo electrónico para enviarte una contraseña
            temporal:
          </div>
          <hr />
          <p />
        </div>
        <FormGroup>
          <Label for={usuarioLabel}>{usuarioLabel}</Label>
          <Input {...usuarioInputProps} />
        </FormGroup>

        <Button
          size="lg"
          className="bg-gradient-theme-left border-0"
          block
          onClick={this.handleSubmit}
        >
          <i class="glyphicon glyphicon-send" aria-hidden="true" />
          {this.renderButtonText()}
        </Button>

        {children}
      </Form>
    );
  }
}

export const STATE_LOGIN = "LOGIN";
export const STATE_SIGNUP = "SIGNUP";

AuthForm.propTypes = {
  authState: PropTypes.oneOf([STATE_LOGIN, STATE_SIGNUP]).isRequired,
  showLogo: PropTypes.bool,
  usuarioLabel: PropTypes.string,
  usuarioInputProps: PropTypes.object,
  contasenaLabel: PropTypes.string,
  contasenaInputProps: PropTypes.object,
  confirmPasswordLabel: PropTypes.string,
  confirmPasswordInputProps: PropTypes.object,
  onLogoClick: PropTypes.func
};

AuthForm.defaultProps = {
  authState: "LOGIN",
  showLogo: true,
  usuarioLabel: "Correo Electrónico",
  usuarioInputProps: {
    type: "email",
    placeholder: "jperez@email.com"
  },
  contasenaLabel: "Contraseña",
  contasenaInputProps: {
    type: "password",
    placeholder: "Contraseña"
  },
  confirmPasswordLabel: "Confirm Password",
  confirmPasswordInputProps: {
    type: "password",
    placeholder: "confirm your password"
  },
  onLogoClick: () => {}
};

export default AuthForm;
